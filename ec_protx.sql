--
-- Table structure for table `ec_protx`
--

CREATE TABLE `ec_protx` (
  `txnid` int(11) NOT NULL default '0',
  `pxid` varchar(64) NOT NULL default '0',
  `amount` decimal(10,2) NOT NULL default '0.00',
  `txauthno` bigint(20) NOT NULL default '0',
  `security_key` varchar(10) default NULL,
  PRIMARY KEY  (`pxid`)
) ENGINE=MyISAM;
